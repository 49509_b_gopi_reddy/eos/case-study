#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<sys/wait.h>
#include<dirent.h>
#include<string.h>
char *file_path;
int count;
char arr[10][20];
int flag;
int choice;

void store_project(void);
void load_project(void);
void menu(void);
void creat_file(void);
void open_file(void);
void display_files(void);
void open_vim(char *file);
void delete_file(void);
void built_file(void);
void clean_file(void);
void run_file(void);
void debug_file( void );

int main(int argc, char *argv[])

{   int ret;
    int fd;
    char file[6]=".proj";
    file_path=strcat(argv[2],file);
    DIR *ptr = opendir(argv[1]);
    if(ptr == NULL)
    {
        ret = mkdir(argv[1],0777);
        if(ret < 0)
        {
            printf("mkdir() is failed\n");
        }
    }

    chdir(argv[1]);

    load_project();

    menu();

    display_files();

    //store_project();
    return 0;
}
void menu(void)
{  
    printf("1.Create file\n");
    printf("2.Open file\n");
    printf("3.delete file\n");
    printf("4.built a file\n");
    printf("5.clean files\n");
    printf("6. Run a file\n");
    printf("7. debug the file\n");
    printf("Choose option: ");
    scanf("%d",&choice);
    switch(choice)
    {
        case 1:
                creat_file();
                break;
        case 2:
                open_file();
                break;
        case 3:
                delete_file();
                break;
        case 4:
                built_file();
                break;
        case 5:
                clean_file();
                break;
        case 6:
                run_file();
                break;
    }
}
void creat_file(void)
{
    int ret;
    int s=0;
    int err ;
    printf("Enter file name: ");
    scanf("%s",arr[count++]);
    open_vim(arr[count-1]);
}
void open_file(void)
{   int i;
    int flag=0;
    char file[20];
    int cmp;
    LABLE:
    printf("enter a file to open: \n");
    scanf("%s",file);
    for (i=0; i<count; i++)
    {
      cmp=strcmp(file,arr[i]);
      printf("cmp: %d\n",cmp);
        if (cmp==0)
        {
             open_vim(file);
              break;
        }   
        
    }
        if (i==count)
        {
                flag++;
                if(flag>3)
                {
                    perror("maxima limit reached\n");
                    exit(1);
                }
                 perror("file not found\n");
                goto LABLE;
        }
}
void delete_file(void)
{
    int cnt=count;
    int i,j=0;
    char temp[10][20];
    char file[20];
    int cmp;
    printf("enter a file to delete: \n");
    scanf("%s",file);
    for (i=0; i<cnt; i++)
    {
      cmp=strcmp(file,arr[i]);
      printf("cmp: %d\n",cmp);
        if (cmp==0)
        {
             unlink(file);
             count--;
        }  
        else
        {
            strcpy(temp[j++],arr[i]);
        }
    }
    for(i=0;i<count;i++)
        strcpy(arr[i],temp[i]);
    printf("file is delete\n");
}
void built_file(void)
{
    int flag=0;
    int ret;
    int err;
    int i;
    int s[count];
    for ( i=0;i<count;i++)
    {
        ret=fork();
        if (ret==0)
        {
            char *args[]={"gcc","-c",arr[i],NULL};
            err=execvp("gcc",args);
            exit(0);
        }
        else
            waitpid(-1, &s[i],0);
    }
    for (i=0; i<count;i++)
    {
        if(s[i]==0)
            flag=1;
        else
        {
            printf("further cannot be proceed as program has errors\n");
            exit(1);
        }
          
    }
    if(flag )
    {
        int cnt = 3;
        int retur;
        char *concat = ".o";
        char *token = strtok(file_path, ".");
        char *args[]={"gcc", "-o", token};
        
        for(int i=0; i<count; i++)
        {
            char *strstr1 = strstr(arr[i], ".h");
            if(strstr1 == NULL)
            {
                char *token = strtok(arr[i], ".");
                char *concat_string = strcat(token, concat);
                args[cnt++] = concat_string;
            }
        }
        args[cnt++] = NULL;
    
        ret=fork();
        if(ret==0)
        {
            err = execvp("gcc", args);
            _exit(0);
        }
        else
            waitpid(-1, &retur,0);
        printf("Return status of run child: %d\n", retur);
    }
}
void display_files(void)
{
    int i = 0;
    printf("...............................\n");
    store_project();
    i=0;
    printf("count:  %d\n",count);
     while( i < count )
    {
        printf("[%d] %s\n",i,arr[i]);
        i++;
    }
    
}

void store_project (void)
{
    int fd = open(file_path,O_WRONLY | O_TRUNC | O_CREAT,0777);
    if(fd != -1)
    {
        write(fd,&count,sizeof(count));
        write(fd,arr,sizeof(arr));
        close(fd);
    }
}

void load_project(void)
{
    int fd = open(file_path,O_RDONLY);
    if(fd != -1)
    {
        read(fd,&count,sizeof(count));
        read(fd,arr, sizeof(arr));
        close(fd);
    }
}
void open_vim(char *file)
{
    int i;
    int ret;
    int s=0;
    int err ;
      printf("file found\n");
            sleep(3);
            ret=fork();
            if (ret==0)
            {
                char *args[]={"vim",file,NULL};
                err=execvp("vim",args);
                if (err<0)
                {
                    printf("not created file");
                    exit(1);
                }
            }
            else
                waitpid(ret,&s,0);
       
}


void clean_file(void)
{
    int ret, err, s;
    char *concat = ".o";
    char *token = strtok(file_path, ".");
    ret = unlink(token);
    ret = fork();
     if (ret == 0)
     {
         char *args[] = {"unlink", token, NULL};
         err = execvp("unlink", args);
         _exit(0);
     }
     else
         wait(&s);
    for(int i=0; i<count; i++)
        {
            char *strstr1 = strstr(arr[i], ".h");
            if(strstr1 == NULL)
            {
                char *token = strtok(arr[i], ".");
                char *concat_string = strcat(token, concat);
                unlink(concat_string);
            }
        }
}


void run_file(void)
{
    char str[32];
    char *ptr, *args[32];
    int i, ret, err, s = 0;
    char *token = strtok(file_path, ".");
    int fd = open(token, O_RDONLY);
    close(fd);
    if(fd != -1)
    {
        while(1) {
            printf("cmd> ");
            scanf("%s", str);
            
            i = 0;
            ptr = strtok(str, " \t");
            args[i++] = ptr;
            do {
                ptr = strtok(NULL, " \t\n");
                args[i++] = ptr;
            }while(ptr != NULL);

            if(strcmp(args[0], "exit") == 0 )
                break;
            else {
                ret = fork();
                if(ret == 0) {
                    err = execvp(args[0], args);
                    if(err < 0) {
                        perror("command not");
                        _exit(1);
                    }
                }
                else {
                    waitpid(ret, &s, 0);
                }
            }
        }
    }
    else
    {
        perror("Executable is not present, Please link your project..!\n");
        _exit(1);
    }
    
}
void debug_file( void )
{
    int err, s=0, ret;
    char *token = strtok(file_path, ".");
    int fd = open(token, O_RDONLY);
    close(fd);
    if(fd != -1)
    {
        printf("Debugger is Opening...\n");
        sleep(2);
        ret = fork();
        if(ret == 0)
        {
            char *args[] = {"gdb", token, NULL};
            execvp("gdb", args);
            _exit(0);
        }
        else
            wait(&s);
    }
    else
    {
        perror("Executable is not created..!\n Create executable and proceed!\n");
        _exit(1);
    }
}

